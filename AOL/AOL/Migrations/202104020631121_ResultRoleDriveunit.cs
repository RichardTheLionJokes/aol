namespace AOL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ResultRoleDriveunit : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DriveUnits",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Results",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CirNumb = c.Byte(nullable: false),
                        Time = c.String(),
                        EventCarId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.EventCars", t => t.EventCarId, cascadeDelete: true)
                .Index(t => t.EventCarId);
            
            CreateTable(
                "dbo.Roles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.RoleUsers",
                c => new
                    {
                        RoleId = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.RoleId, t.UserId })
                .ForeignKey("dbo.Roles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.RoleId)
                .Index(t => t.UserId);
            
            AddColumn("dbo.Cars", "Brand", c => c.String());
            AddColumn("dbo.Cars", "Model", c => c.String());
            AddColumn("dbo.Cars", "Year", c => c.Int(nullable: false));
            AddColumn("dbo.Cars", "DriveUnitId", c => c.Int(nullable: false));
            AddColumn("dbo.Events", "Date", c => c.DateTime(nullable: false));
            AddColumn("dbo.Events", "Place", c => c.String());
            AddColumn("dbo.Events", "Description", c => c.String());
            AddColumn("dbo.Users", "Phone", c => c.String());
            AddColumn("dbo.Users", "Email", c => c.String());
            AddColumn("dbo.Users", "Address", c => c.String());
            CreateIndex("dbo.Cars", "DriveUnitId");
            AddForeignKey("dbo.Cars", "DriveUnitId", "dbo.DriveUnits", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.RoleUsers", "User_Id", "dbo.Users");
            DropForeignKey("dbo.RoleUsers", "Role_Id", "dbo.Roles");
            DropForeignKey("dbo.Results", "EventCarId", "dbo.EventCars");
            DropForeignKey("dbo.Cars", "DriveUnitId", "dbo.DriveUnits");
            DropIndex("dbo.RoleUsers", new[] { "User_Id" });
            DropIndex("dbo.RoleUsers", new[] { "Role_Id" });
            DropIndex("dbo.Results", new[] { "EventCarId" });
            DropIndex("dbo.Cars", new[] { "DriveUnitId" });
            DropColumn("dbo.Users", "Address");
            DropColumn("dbo.Users", "Email");
            DropColumn("dbo.Users", "Phone");
            DropColumn("dbo.Events", "Description");
            DropColumn("dbo.Events", "Place");
            DropColumn("dbo.Events", "Date");
            DropColumn("dbo.Cars", "DriveUnitId");
            DropColumn("dbo.Cars", "Year");
            DropColumn("dbo.Cars", "Model");
            DropColumn("dbo.Cars", "Brand");
            DropTable("dbo.RoleUsers");
            DropTable("dbo.Roles");
            DropTable("dbo.Results");
            DropTable("dbo.DriveUnits");
        }
    }
}
