namespace AOL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UsersCarsEvents : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Cars",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.EventCars",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EventId = c.Int(nullable: false),
                        CarId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cars", t => t.CarId, cascadeDelete: true)
                .ForeignKey("dbo.Events", t => t.EventId, cascadeDelete: true)
                .Index(t => t.EventId)
                .Index(t => t.CarId);
            
            CreateTable(
                "dbo.Events",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Cars", "UserId", "dbo.Users");
            DropForeignKey("dbo.EventCars", "EventId", "dbo.Events");
            DropForeignKey("dbo.EventCars", "CarId", "dbo.Cars");
            DropIndex("dbo.EventCars", new[] { "CarId" });
            DropIndex("dbo.EventCars", new[] { "EventId" });
            DropIndex("dbo.Cars", new[] { "UserId" });
            DropTable("dbo.Users");
            DropTable("dbo.Events");
            DropTable("dbo.EventCars");
            DropTable("dbo.Cars");
        }
    }
}
