﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankLibrary
{
    public class EventCar
    {
        public int Id { get; set; }
        public int EventId { get; set; }
        public virtual Event Event { get; set; }
        public int CarId { get; set; }
        public virtual Car Car { get; set; }

        public virtual ICollection<Result> Results { get; set; }
    }
}
