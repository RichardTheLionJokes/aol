﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankLibrary
{
    public class Car
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Brand { get; set; }
        public string Model { get; set; }
        public int Year { get; set; }
        public int UserId { get; set; }
        public virtual User User { get; set; }
        public int DriveUnitId { get; set; }
        public virtual DriveUnit DriveUnit { get; set; }

        public virtual ICollection<EventCar> EventCars { get; set; }
    }
}
