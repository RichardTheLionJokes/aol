﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankLibrary
{
    public class Result
    {
        public int Id { get; set; }
        public byte CirNumb { get; set; }
        public string Time { get; set; }

        public int EventCarId { get; set; }
        public virtual EventCar EventCar { get; set; }
    }
}
